package uiautomator;

import android.util.Log;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class Uiautomatortest extends UiAutomatorTestCase {
    public void testStartApp() throws Exception {
    	Log.d("testUI", "test automator start.");

    	// get device
    	// UiDevice myDevice = getUiDevice();
    	// Log.d("testUI", myDevice.toString());
    	
    	// check kantei list
    	UiObject kanteiText = new UiObject(new UiSelector().className("android.widget.TextView").textContains("鑑定一覧"));
    	if ( ! kanteiText.exists())
    	{
			Log.d("testUI", "not on KanteiMenu : " + String.valueOf( ! kanteiText.exists()));
    		// check index button
			UiObject indexButton = new UiObject(new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/back"));
			if (indexButton.exists())
			{
				// click and wait next screen
				indexButton.clickAndWaitForNewWindow();

				// enter kantei button
				UiObject enterButton = new UiObject(new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/menu_btn1"));
				enterButton.clickAndWaitForNewWindow();
			}
			else
			{
				// check go back menu button
				UiObject backButton = new UiObject(new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/menu_btn"));
				if (backButton.exists())
				{
					// back to MENU button exists
					backButton.clickAndWaitForNewWindow();
				}
				else
				{
					// normal index menu
					UiObject enterButton = new UiObject(new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/menu_btn1"));
					enterButton.clickAndWaitForNewWindow();
				}
			}
    	}
    	

    	// on 鑑定一覧 menu
    	// get scroll view of list
    	UiSelector scrollSelect = new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/scroll");
    	UiScrollable kanteiScrollMenu = new UiScrollable(scrollSelect);
    	
    	// loop each kantei menu
    	for (int i = 0; i < 100; i++)
    	{
			UiObject kMenu = new UiObject(new UiSelector().resourceId("jp.co.rensa.kusaka.fortune:id/rl").childSelector(new UiSelector().className("android.widget.TextView").index(i)));
			kanteiScrollMenu.scrollIntoView(kMenu);
			System.out.println("text is : " + kMenu.getText());

    	}
    	
    }
}